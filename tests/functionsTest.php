<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {

    public function test_getOneDino(){
        $dino = getOneDino("velociraptor");
        $this->assertEquals(True,property_exists($dino,'name'));
        $this->assertEquals(True,property_exists($dino,'name_meaning'));
        $this->assertEquals(True,property_exists($dino,'height'));
        $this->assertEquals(True,property_exists($dino,'weight'));
        $this->assertEquals(True,property_exists($dino,'length'));
        $this->assertEquals(True,property_exists($dino,'diet'));
    }

    public function test_getDino(){
        $dinos = getDino();
        foreach ($dinos as $dino){ 
            $this->assertEquals(True,property_exists($dino,'name'));
            $this->assertEquals(True,property_exists($dino,'name_meaning'));
            $this->assertEquals(True,property_exists($dino,'height'));
            $this->assertEquals(True,property_exists($dino,'weight'));
            $this->assertEquals(True,property_exists($dino,'length'));
            $this->assertEquals(True,property_exists($dino,'diet'));
        }
    }

    public function test_getAleatoireDino(){
        $dinos = getAleatoireDino();
        $this->assertEquals(3,count($dinos));
        foreach ($dinos as $dino){ 
            $this->assertEquals(True,property_exists($dino,'name'));
            $this->assertEquals(True,property_exists($dino,'name_meaning'));
            $this->assertEquals(True,property_exists($dino,'height'));
            $this->assertEquals(True,property_exists($dino,'weight'));
            $this->assertEquals(True,property_exists($dino,'length'));
            $this->assertEquals(True,property_exists($dino,'diet'));
        }
    }

}