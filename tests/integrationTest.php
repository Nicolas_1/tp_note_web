<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_listedino()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Velociraptor", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_dino()
    {
        $response = $this->make_request("GET", "/dinosaurs/velociraptor");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Velociraptor", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

}